# Configure AWS provider
provider "aws" {
  region = "us-east-1" # Replace with your desired region
}

# Create VPC
resource "aws_vpc" "my_vpc" {
  cidr_block = "10.0.0.0/16" # Replace with desired CIDR block

  tags = {
    Name = "my-vpc"
  }
}

# Create public subnet
resource "aws_subnet" "public_subnet" {
  vpc_id     = aws_vpc.my_vpc.id
  cidr_block = "10.0.1.0/24" # Replace with desired CIDR block
  map_public_ip_on_launch = true

  tags = {
    Name = "public-subnet"
  }
}

# Create Internet Gateway
resource "aws_internet_gateway" "my_igw" {
  vpc_id = aws_vpc.my_vpc.id

  tags = {
    Name = "my-igw"
  }
}

# Create network ACL for public subnet
resource "aws_network_acl" "public_subnet_acl" {
  vpc_id = aws_vpc.my_vpc.id

  egress {
    rule_no    = 200
    protocol   = "tcp"
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 65535
  }

  ingress {
    rule_no    = 100
    protocol   = "tcp"
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 65535
  }

  subnet_ids = [aws_subnet.public_subnet.id]
}

# Update route table to allow outbound internet traffic
resource "aws_route_table" "public_subnet_route_table" {
  vpc_id = aws_vpc.my_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my_igw.id
  }

  tags = {
    Name = "public-subnet-route-table"
  }
}

# Associate public subnet with network ACL
resource "aws_network_acl_association" "public_subnet_acl_association" {
  network_acl_id = aws_network_acl.public_subnet_acl.id
  subnet_id      = aws_subnet.public_subnet.id
}

# Associate public subnet with updated route table
resource "aws_route_table_association" "public_subnet_route_table_association" {
  subnet_id      = aws_subnet.public_subnet.id
  route_table_id = aws_route_table.public_subnet_route_table.id
}



resource "tls_private_key" "my_ssh_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_file" "my_ssh_key_pub" {
  content  = tls_private_key.my_ssh_key.public_key_openssh
  filename = "my_ssh_key.pub"
}

resource "aws_key_pair" "my_key_pair" {
  key_name   = "my_key_pair"
  public_key = local_file.my_ssh_key_pub.content
}

# Create security group for EC2 Instance Connect
resource "aws_security_group" "connect_sg" {
  name_prefix = "ec2-instance-connect-"
  description = "Security group for EC2 Instance Connect"
  vpc_id      = aws_vpc.my_vpc.id

  # Allow SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow SSH access from your VPC
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["10.0.0.0/16"]
  }

  # Allow HTTP access from anywhere
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow outbound traffic to anywhere
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}






# Create EC2 instance in public subnet
resource "aws_instance" "ubuntu" {
  ami           = "ami-007855ac798b5175e" # Replace with desired Ubuntu AMI ID
  instance_type = "t2.micro" # Replace with desired instance type
  key_name      = aws_key_pair.my_key_pair.key_name # Replace with name of your SSH keypair
  subnet_id     = aws_subnet.public_subnet.id
  vpc_security_group_ids = [aws_security_group.connect_sg.id] # Associate security group with instance

  # Configure user data to install software on instance launch
user_data = <<-EOF
            #!/bin/bash
            # Install Docker
            sudo apt-get update
            sudo apt-get install -y apt-transport-https ca-certificates curl gnupg lsb-release
            curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
            echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
            sudo apt-get update
            sudo apt-get install -y docker-ce docker-ce-cli containerd.io

            # Add current user to docker group and change ownership of docker socket
            sudo usermod -aG docker $USER
            sudo chown $USER:docker /var/run/docker.sock
            
            # Install Docker Compose
            sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
            sudo chmod +x /usr/local/bin/docker-compose
            
            # Install nginx
            sudo apt-get update
            sudo apt-get install -y nginx
            # Enable and start Nginx service
            sudo systemctl enable nginx
            sudo systemctl start nginx

            # Install git
            sudo apt-get update
            sudo apt-get install -y git

            # Install Gitlab-Runner
            curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
            sudo apt-get install gitlab-runner
            sudo usermod -aG docker gitlab-runner
            sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
            sudo gitlab-runner start

            # Install nano
            apt-get update
            apt-get install -y nano
            EOF
  tags = {
    Name        = "EC2_Template"
    Environment = "dev"
  }
}

# Output instance public IP address
output "public_ip" {
  value = aws_instance.ubuntu.public_ip
}
